# Styleguide for [@litc0de/xstyles](gitlab.com/litc0de/xstyles)

Deployed here: https://litc0de.gitlab.io/styleguide

## Project setup
```sh
npm i
```

### Compiles and hot-reloads for development
```sh
npm run serve
```

### Compiles and minifies for production
```sh
npm run build
```

### Run your tests
```sh
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```
