import { shallowMount, createLocalVue } from '@vue/test-utils';

import ApiExplorerTable from '../ApiExplorerTable.vue';

const localVue = createLocalVue();

describe('ApiExplorerTable', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(ApiExplorerTable, {
      localVue,
      propsData: {
        items: {
          prop1: { default: false, type: Boolean },
          prop2: { default: 'test', type: String },
          prop3: { required: true, type: Array },
          prop4: { default: { test: 'test' }, type: Object },
          prop5: { default: '/route', type: [Object, String] },
        },
      },
    });
  });

  describe('computed', () => {
    describe('headers', () => {
      it('returns a headers array', () => {
        const expectation = ['name', 'default', 'type', 'required'];

        expect(wrapper.vm.headers).toEqual(expectation);
      });
    });

    describe('items', () => {
      it('returns the table body data', () => {
        const expectation = [
          { name: 'prop1', default: 'false', type: 'Boolean' },
          { name: 'prop2', default: 'test', type: 'String' },
          { name: 'prop3', required: 'true', type: 'Array' },
          { name: 'prop4', default: '[object Object]', type: 'Object' },
          { name: 'prop5', default: '/route', type: '[Object, String]' },
        ];

        expect(wrapper.vm.body).toEqual(expectation);
      });
    });
  });
});
