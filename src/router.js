import Vue from 'vue';
import Router from 'vue-router';
import baseComponentNames from '@/baseComponents';
import componentNames from '@/components';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: () => import('@/views/Home.vue'),
    },
    {
      path: '/getting-started',
      name: 'GettingStarted',
      component: () => import('@/general/GettingStarted.vue'),
    },
    {
      path: '/headings',
      name: 'Headings',
      component: () => import('@/general/Headings.vue'),
    },
    {
      path: '/text',
      name: 'Text',
      component: () => import('@/general/Text.vue'),
    },
    {
      path: '/variables',
      name: 'Variables',
      component: () => import('@/general/Variables/Index.vue'),
    },
    ...baseComponentNames.map(component => ({
      path: `/base-components/${component}`,
      name: component,
      component: () => import(`@/baseComponents/${component}/Index.vue`),
    })),

    ...componentNames.map(component => ({
      path: `/components/${component}`,
      name: component,
      component: () => import(`@/components/${component}/Index.vue`),
    })),
  ],
});
