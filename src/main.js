import Vue from 'vue';
import App from '@/App.vue';
import router from '@/router';
import '@/registerServiceWorker';
import '@/plugins/xstyles';
import 'prismjs/themes/prism-tomorrow.css';

import ExampleCard from '@/ExampleCard/ExampleCard.vue';
import ApiExplorer from '@/ApiExplorer/ApiExplorer.vue';

Vue.config.productionTip = false;

Vue.component('example-card', ExampleCard);
Vue.component('api-explorer', ApiExplorer);

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
