const context = require.context(
  '.',
  true,
  /^(?!.*(?:__tests__|test.js$|spec.js$|index.js$)).*[iI]ndex(?:.vue$|.js$)/,
);

export default context.keys().map(file => context(file).default.name);
