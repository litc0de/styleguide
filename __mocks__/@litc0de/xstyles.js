export default {};

export const components = {
  test: {
    name: 'test',
    render(createElement) {
      return createElement(
        'div',
        this.$slots.default,
      );
    },
  },
};
